package com.dant.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Index implements Serializable{
    public Column column;
    public int columnsPosition;
    public Map<String, List<Object[]>> rows = new HashMap<>();

    public void addColumn(Table table, int position){
        column = table.columns.get(position);
        columnsPosition = position;
    }

    public List<Object[]> get(String key){
        return rows.get(key);
    }

    public Column getColumn(){
        return this.column;
    }

    public Map<String, List<Object[]>> getRows(){
        return this.rows;
    }
    public void put(Object[] data){
        String key = data[columnsPosition].toString();
        List<Object[]> row = rows.getOrDefault(key, new ArrayList<>());
        row.add(data);
        rows.putIfAbsent(key, row);
    }
}