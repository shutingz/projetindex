package com.dant.entity;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;

import com.opencsv.CSVReader;

public class Tables {

    private ArrayList<Table> allTables;

    public Tables() {
        allTables = new ArrayList<Table>();
    }

   
    public void startIndexing(String fileName, String tableName) throws IOException {

        // Files related vars
        fileName = "./yellow_tripdata_2019-01.csv";
        FileInputStream fis = new FileInputStream(fileName);
        InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
        CSVReader reader = new CSVReader(isr);

        String outputFilePath = "./src/main/resources/csv/data.bin";
        DataOutputStream out = new DataOutputStream((new BufferedOutputStream(new FileOutputStream(outputFilePath))));

        // Reading CSV vars
        String[] lineArray;
        Object[] castedLine;
        int i, headerLength;

        Table t;
        if ((t = getTableByName(tableName)) == null){
            reader.close();
            out.close();
            return;
        }
        ArrayList<Column> indexedColumns = t.getColumns();

        try {
            lineArray = reader.readNext();
            headerLength = lineArray.length;
            if (lineArray.length != getTableByName(tableName).getColumns().size()) {
                System.out.println("Erreur");
                reader.close();
                out.close();
                return;
            }
            for (i = 0; i < headerLength; i++) {
                t.getColumn(lineArray[i]).setNum(i);
            }
            t.sortByNum();
            long noLine = 0;
            while ((lineArray = reader.readNext()) != null) {
                castedLine = new Object[headerLength];
                for (i = 0; i < headerLength; i++) {
                    castedLine[i] = lineArray[i];
                    out.writeUTF(!lineArray[i].isEmpty() ? (String) castedLine[i] : "");
                }
                for (Column c : indexedColumns) {
                    c.index(castedLine[c.getNum()], (int) noLine);
                }
            }
        } catch (CancellationException e) {
            e.printStackTrace();
            System.out.println("Erreur");
        } catch (Exception e) {
            System.out.println("Erreur");
            e.printStackTrace();
        }
    }

    public ArrayList<Table> getTables() {
        return allTables;
    }

    public Table getTableByName(String name) {
        for (Table t : allTables) {
            if (t.getName().equals(name)) {
                return t;
            }
        }
        return null;
    }

    public void addTable(Table t) {
        allTables.add(t);
    }
}

