package com.dant.entity;

import java.io.Serializable;
import java.util.Objects;

public class Taxi implements Serializable {
    
    private int VendorID;
    private String tpep_pickup_datetime;
    private String tpep_dropoff_datetime;
    private int passenger_count;
    private float trip_distance;
    private int RatecodeID;
    private char store_and_fwd_flag;
    private int PULocationID;
    private int DOLocationID;
    private int payment_type;
    private float fare_amount;
    private float extra;
    private float mta_tax;
    private float tip_amount;
    private float tolls_amount; 
    private float total_amount;
    private float improvement_surcharge;
    private String congestion_surcharge;

    public Taxi() {
    }

    public Taxi(int VendorID, String tpep_pickup_datetime, String tpep_dropoff_datetime, int passenger_count, float trip_distance, int RatecodeID, char store_and_fwd_flag, int PULocationID, int DOLocationID, int payment_type, float fare_amount, float extra, float mta_tax, float tip_amount, float tolls_amount, float total_amount, float improvement_surcharge, String congestion_surcharge) {
        this.VendorID = VendorID;
        this.tpep_pickup_datetime = tpep_pickup_datetime;
        this.tpep_dropoff_datetime = tpep_dropoff_datetime;
        this.passenger_count = passenger_count;
        this.trip_distance = trip_distance;
        this.RatecodeID = RatecodeID;
        this.store_and_fwd_flag = store_and_fwd_flag;
        this.PULocationID = PULocationID;
        this.DOLocationID = DOLocationID;
        this.payment_type = payment_type;
        this.fare_amount = fare_amount;
        this.extra = extra;
        this.mta_tax = mta_tax;
        this.tip_amount = tip_amount;
        this.tolls_amount = tolls_amount;
        this.total_amount = total_amount;
        this.improvement_surcharge = improvement_surcharge;
        this.congestion_surcharge = congestion_surcharge;
    }

    public int getVendorID() {
        return this.VendorID;
    }

    public void setVendorID(int VendorID) {
        this.VendorID = VendorID;
    }

    public String getTpep_pickup_datetime() {
        return this.tpep_pickup_datetime;
    }

    public void setTpep_pickup_datetime(String tpep_pickup_datetime) {
        this.tpep_pickup_datetime = tpep_pickup_datetime;
    }

    public String getTpep_dropoff_datetime() {
        return this.tpep_dropoff_datetime;
    }

    public void setTpep_dropoff_datetime(String tpep_dropoff_datetime) {
        this.tpep_dropoff_datetime = tpep_dropoff_datetime;
    }

    public int getPassenger_count() {
        return this.passenger_count;
    }

    public void setPassenger_count(int passenger_count) {
        this.passenger_count = passenger_count;
    }

    public float getTrip_distance() {
        return this.trip_distance;
    }

    public void setTrip_distance(float trip_distance) {
        this.trip_distance = trip_distance;
    }

    public int getRatecodeID() {
        return this.RatecodeID;
    }

    public void setRatecodeID(int RatecodeID) {
        this.RatecodeID = RatecodeID;
    }

    public char getStore_and_fwd_flag() {
        return this.store_and_fwd_flag;
    }

    public void setStore_and_fwd_flag(char store_and_fwd_flag) {
        this.store_and_fwd_flag = store_and_fwd_flag;
    }

    public int getPULocationID() {
        return this.PULocationID;
    }

    public void setPULocationID(int PULocationID) {
        this.PULocationID = PULocationID;
    }

    public int getDOLocationID() {
        return this.DOLocationID;
    }

    public void setDOLocationID(int DOLocationID) {
        this.DOLocationID = DOLocationID;
    }

    public int getPayment_type() {
        return this.payment_type;
    }

    public void setPayment_type(int payment_type) {
        this.payment_type = payment_type;
    }

    public float getFare_amount() {
        return this.fare_amount;
    }

    public void setFare_amount(float fare_amount) {
        this.fare_amount = fare_amount;
    }

    public float getExtra() {
        return this.extra;
    }

    public void setExtra(float extra) {
        this.extra = extra;
    }

    public float getMta_tax() {
        return this.mta_tax;
    }

    public void setMta_tax(float mta_tax) {
        this.mta_tax = mta_tax;
    }

    public float getTip_amount() {
        return this.tip_amount;
    }

    public void setTip_amount(float tip_amount) {
        this.tip_amount = tip_amount;
    }

    public float getTolls_amount() {
        return this.tolls_amount;
    }

    public void setTolls_amount(float tolls_amount) {
        this.tolls_amount = tolls_amount;
    }

    public float getTotal_amount() {
        return this.total_amount;
    }

    public void setTotal_amount(float total_amount) {
        this.total_amount = total_amount;
    }

    public float getImprovement_surcharge() {
        return this.improvement_surcharge;
    }

    public void setImprovement_surcharge(float improvement_surcharge) {
        this.improvement_surcharge = improvement_surcharge;
    }

    public String getCongestion_surcharge() {
        return this.congestion_surcharge;
    }

    public void setCongestion_surcharge(String congestion_surcharge) {
        this.congestion_surcharge = congestion_surcharge;
    }

    public Taxi VendorID(int VendorID) {
        this.VendorID = VendorID;
        return this;
    }

    public Taxi tpep_pickup_datetime(String tpep_pickup_datetime) {
        this.tpep_pickup_datetime = tpep_pickup_datetime;
        return this;
    }

    public Taxi tpep_dropoff_datetime(String tpep_dropoff_datetime) {
        this.tpep_dropoff_datetime = tpep_dropoff_datetime;
        return this;
    }

    public Taxi passenger_count(int passenger_count) {
        this.passenger_count = passenger_count;
        return this;
    }

    public Taxi trip_distance(float trip_distance) {
        this.trip_distance = trip_distance;
        return this;
    }

    public Taxi RatecodeID(int RatecodeID) {
        this.RatecodeID = RatecodeID;
        return this;
    }

    public Taxi store_and_fwd_flag(char store_and_fwd_flag) {
        this.store_and_fwd_flag = store_and_fwd_flag;
        return this;
    }

    public Taxi PULocationID(int PULocationID) {
        this.PULocationID = PULocationID;
        return this;
    }

    public Taxi DOLocationID(int DOLocationID) {
        this.DOLocationID = DOLocationID;
        return this;
    }

    public Taxi payment_type(int payment_type) {
        this.payment_type = payment_type;
        return this;
    }

    public Taxi fare_amount(float fare_amount) {
        this.fare_amount = fare_amount;
        return this;
    }

    public Taxi extra(float extra) {
        this.extra = extra;
        return this;
    }

    public Taxi mta_tax(float mta_tax) {
        this.mta_tax = mta_tax;
        return this;
    }

    public Taxi tip_amount(float tip_amount) {
        this.tip_amount = tip_amount;
        return this;
    }

    public Taxi tolls_amount(float tolls_amount) {
        this.tolls_amount = tolls_amount;
        return this;
    }

    public Taxi total_amount(float total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public Taxi improvement_surcharge(float improvement_surcharge) {
        this.improvement_surcharge = improvement_surcharge;
        return this;
    }

    public Taxi congestion_surcharge(String congestion_surcharge) {
        this.congestion_surcharge = congestion_surcharge;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Taxi)) {
            return false;
        }
        Taxi taxi = (Taxi) o;
        return VendorID == taxi.VendorID && Objects.equals(tpep_pickup_datetime, taxi.tpep_pickup_datetime) && Objects.equals(tpep_dropoff_datetime, taxi.tpep_dropoff_datetime) && passenger_count == taxi.passenger_count && trip_distance == taxi.trip_distance && RatecodeID == taxi.RatecodeID && store_and_fwd_flag == taxi.store_and_fwd_flag && PULocationID == taxi.PULocationID && DOLocationID == taxi.DOLocationID && payment_type == taxi.payment_type && fare_amount == taxi.fare_amount && extra == taxi.extra && mta_tax == taxi.mta_tax && tip_amount == taxi.tip_amount && tolls_amount == taxi.tolls_amount && total_amount == taxi.total_amount && improvement_surcharge == taxi.improvement_surcharge && Objects.equals(congestion_surcharge, taxi.congestion_surcharge);
    }

   //@Override
   //public int hashCode() {
   //    return Objects.hash(VendorID, tpep_pickup_datetime, tpep_dropoff_datetime, passenger_count, trip_distance, RatecodeID, store_and_fwd_flag, PULocationID, DOLocationID, payment_type, fare_amount, extra, mta_tax, tip_amount, tolls_amount, total_amount, improvement_surcharge, congestion_surcharge);
   //}

    @Override
    public String toString() {
        return "{" +
            "\n VendorID='" + getVendorID() + "'" +
            "\n, tpep_pickup_datetime='" + getTpep_pickup_datetime() + "'" +
            "\n, tpep_dropoff_datetime='" + getTpep_dropoff_datetime() + "'" +
            "\n, passenger_count='" + getPassenger_count() + "'" +
            "\n, trip_distance='" + getTrip_distance() + "'" +
            "\n, RatecodeID='" + getRatecodeID() + "'" +
            "\n, store_and_fwd_flag='" + getStore_and_fwd_flag() + "'" +
            "\n, PULocationID='" + getPULocationID() + "'" +
            "\n, DOLocationID='" + getDOLocationID() + "'" +
            "\n, payment_type='" + getPayment_type() + "'" +
            "\n, fare_amount='" + getFare_amount() + "'" +
            "\n, extra='" + getExtra() + "'" +
            "\n, mta_tax='" + getMta_tax() + "'" +
            "\n, tip_amount='" + getTip_amount() + "'" +
            "\n, tolls_amount='" + getTolls_amount() + "'" +
            "\n, total_amount='" + getTotal_amount() + "'" +
            "\n, improvement_surcharge='" + getImprovement_surcharge() + "'" +
            "\n, congestion_surcharge='" + getCongestion_surcharge() + "'" +
            "\n}";
    }
   

 
	
}