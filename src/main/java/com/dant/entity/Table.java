package com.dant.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Table implements Serializable {
    public String name;
    public ArrayList<Column> columns = new ArrayList<>();
    public ArrayList<Index> indexs = new ArrayList<>();

    public Table(String name){
        this.name = name;
    }

    public Table(String argName, List<Column> argColumns){
        this.name = argName;
        this.columns = argColumns;
    }

    public List<Index> getIndexs(){
        return this.indexs;
    }

    public void put(Object[] line){
        for(Index index: indexs){
            index.put(line);
        }
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<Column> getColumns(){
        return this.columns;
    }

    public Column getColumn(String name){
        int i =0;
        for(i = 0; i < columns.size(); i++){
            if(columns.get(i).getName() == name){
                return columns.get(i);
            }
        }
        return new Column(name);
    }

    public void sortByNum(){
        Collections.sort(columns,
        (col1, col2) -> col1.getNum()
                - col2.getNum());
    }
    
    public void addColumn(Column column){
		columns.add(column);
    }
    
    public void addIndex(Index index){
		this.indexs.add(index);
    }

    public List<Object[]> get(String key){
        return indexs.get(0).get(key);
    }
    @Override
    public String toString() {
        return getName() + getColumns() + getIndexs();
    }

}