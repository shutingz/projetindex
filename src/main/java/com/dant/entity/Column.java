package com.dant.entity;

import java.io.Serializable;
import java.util.Objects;

public class Column implements Serializable {
    private String name;
    private String type;
    private int num;
    private int index;

    public Column() {
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Column(String name, String type) {
        this.name = name;
        this.type = type;
        index = -1;
    }

    public void index(Object o, int noLine) {
    }

    public Column(String name2) {
        this.name = name2;
        type = "String";
        num = -1;
	}

	public void setName(String name) {
        this.name = name;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Column name(String name) {
        this.name = name;
        return this;
    }

    public Column type(String type) {
        this.type = type;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Column)) {
            return false;
        }
        Column column = (Column) o;
        return Objects.equals(name, column.name) && Objects.equals(type, column.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
    
    public String getName(){
        if(name == null) return null;
        return name;
    }

    public String getType(){
        if(type == null) return null;
        return type;
    }

}