package com.dant.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.dant.entity.Account;
import com.dant.entity.Column;
import com.dant.entity.Table;
import com.dant.entity.Tables;
import com.dant.entity.Taxi;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;  
import org.glassfish.jersey.media.multipart.FormDataParam;  


/**
* Created by pitton on 2017-02-20.
*/
@Path("/api/test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TestEndpoint {

	Tables tables = new Tables();
	
	@GET
	@Path("ping")
	public String getServerTime() {
		System.out.println("RESTful Service 'MessageService' is running ==> ping");
		return "received ping on " + new Date().toString();
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String helloWorld() {
		return "Hello World";
	}
	
	@GET
	@Path("/list")
	public List<String> getListInParams(@QueryParam("ids") List<String> ids) {
		System.out.println(ids);
		return ids;
	}
	
	@POST
	@Path("/entity")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Account getAccount(Account account) {
		System.out.println("Received account " + account);
		account.setUpdated(System.currentTimeMillis());
		System.out.println(account);
		return account;
	}
	
	
	// @PUT
	// @Path("/tables") public void createTables(
	// @ApiParam(value = "content", required = true) ArrayList<TableEntity> allTables,
	// @DefaultValue("null") @HeaderParam("InternalToken") String InternalToken,
	// final @Suspended AsyncResponse responseToClient) { 
	// 	// si la requète vient
	// 	//d'un endpoint, pas besoin de valider 
	// 	boolean addImmediatly =InternalToken.equals(Database.getInstance().config.SuperSecretPassphrase);
	// 	Controller.addTables(allTables, addImmediatly);
	// 	if (!addImmediatly && Database.getInstance().allNodes.size() > 0) {
	// 		Gson gson = new Gson();
	// 		String body = gson.toJson(allTables);
	// 	} 
	// 	else { responseToClient.resume(newHttpResponse("ok")); }
	// }
	
	
	@GET
	@Path("/exception")
	public Response exception() {
		throw new RuntimeException("Mon erreur");
	}
	
	@GET
	@Path("/panda")
	public Account getAccount() {
		Account a = new Account("shushu@gmail.com");
		System.out.println("Received account " + a);
		a.setUpdated(System.currentTimeMillis());
		return a;
	}
	
	@POST
	@Path("/redirect")
	public Response yourAPIMethod() throws URISyntaxException {
		URI targetURIForRedirection = new URI("https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page");
		return Response.seeOther(targetURIForRedirection).build();
	}
	
	@POST
	@Path("/taxiscsv")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public String getTaxiText(String taxis) {
		String res = csvToJson(taxis);
		try {
			Response
			.seeOther(new URI("http://localhost:8080/api/test/entitys"))
			.entity(res)
			.type(MediaType.APPLICATION_JSON)
			.build();
			return res;
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "error";
		}
	}
	
	@GET
	@Path("/entitys")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String getTaxi(List<Taxi> taxis) {
		String res ="";
		for (Taxi taxi : taxis) {
			System.out.println("Received taxi " + taxi.getVendorID());
			res += taxi +"\n";
		}
		return res;
	}

	@POST
    @Path("/csv")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String getCSV(String csvName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("./src/main/java/com/dant/app/"+csvName+".csv"));
        List<String> lines = new ArrayList<>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        System.out.println("helloooooooo");
		System.out.println(lines.get(0));
		reader.close();
		return lines.get(0);
	}

	@GET
    @Path("/tables/{tableName}")
    public String getTable(@PathParam("tableName") String tableName) {
        return tables.getTableByName(tableName).toString();
	}
	
	@DELETE
    @Path("/tables/{tableName}")
    public Boolean deleteTable(@PathParam("tableName") String tableName) {
        return tables.getTables().remove(tables.getTableByName(tableName));
	}
	
	@POST
	@Path("/createTable")
	public Response createTable(String body)  {
		JsonObject tableLayout = new JsonParser().parse(body).getAsJsonObject();
		String tableName;
		tableName = tableLayout.get("name").getAsString();
		Table table = new Table(tableName);
		System.out.println(table);

		Column c;
		Set<Map.Entry<String, JsonElement>> entries = tableLayout.entrySet();
      	for(Map.Entry<String, JsonElement> entry: entries) {
			  if(entry.getKey().equals("name")){
				  continue;
			  }
			 System.out.println(entry.getKey());
			 c = new Column(entry.getKey(),"String");
			table.addColumn(c);
		}

		this.tables.addTable(table);
		System.out.println(table);
		return Response.status(201).entity("Table \"" + tableName + "\" is created").build();
	}

	@POST
	@Path("/startIndexing")
	public Response startIndexing() throws IOException {
		tables.startIndexing("./", "nyc");
		return Response.status(200).entity("Started indexing").build();
	}

	@POST  
    @Path("/upload")  
    @Consumes(MediaType.MULTIPART_FORM_DATA)  
    public Response uploadFile(  
        @FormDataParam("file") InputStream uploadedInputStream,  
        @FormDataParam("file") FormDataContentDisposition fileDetail) {  
        String fileLocation = "./" + fileDetail.getFileName();  
        try {  
            FileOutputStream out = new FileOutputStream(new File(fileLocation));  
            int read = 0;  
            byte[] bytes = new byte[1024];  
            out = new FileOutputStream(new File(fileLocation));  
            while ((read = uploadedInputStream.read(bytes)) != -1) {  
                out.write(bytes, 0, read);  
            }  
            out.flush();  
            out.close();  
        } catch (IOException e) {e.printStackTrace();}  
        String output = "File successfully uploaded to : " + fileLocation;  
        return Response.status(200).entity(output).build();  
    }  
	
	public static String csvToJson(String csvstr){
		
		String str[] = csvstr.split("\n");
		List<String> csv = new ArrayList<String>();
		csv = Arrays.asList(str);
		//remove empty lines
		//this will affect permanently the list. 
		//be careful if you want to use this list after executing this method
		csv.removeIf(e -> e.trim().isEmpty());
		
		//csv is empty or have declared only columns
		if(csv.size() <= 1){
			return "[]";
		}
		
		//get first line = columns names
		String[] columns = csv.get(0).split(",");
		columns[columns.length-1] = columns[columns.length-1].replace("\r", "");
		
		//get all rows
		StringBuilder json = new StringBuilder("[\n");
		csv.subList(1, csv.size()) //substring without first row(columns)
		.stream()
		.map(e -> e.split(","))
		.filter(e -> e.length == columns.length) //values size should match with columns size
		.forEach(row -> {
			
			json.append("\t{\n");
			
			for(int i = 0; i < columns.length; i++){
				//test needed for last empty cell
				if(row[i].equals("\r")){
					row[i] = "NaN";
				}
				json.append("\t\t\"")
				.append(columns[i])
				.append("\" : \"")
				.append(row[i])
				.append("\",\n"); //comma-1
			}
			
			
			//replace comma-1 with \n
			json.replace(json.lastIndexOf(","), json.length(), "\n");
			
			json.append("\t},"); //comma-2
			
		});
		
		//remove comma-2
		json.replace(json.lastIndexOf(","), json.length(), "");
		
		json.append("\n]");
		
		return json.toString();
		
	}
}
			