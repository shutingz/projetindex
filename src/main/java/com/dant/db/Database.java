package com.dant.db;

import java.util.ArrayList;

import com.dant.entity.Table;

public final class Database {

    private ArrayList<Table> allTables;

    private Database(){
        allTables = new ArrayList<>();
    }

    public ArrayList<Table> getAllTables() {
        return allTables;
    }
}